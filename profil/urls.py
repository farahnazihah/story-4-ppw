from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='profil'),
    path('story1', views.story1, name='story1'),
    path('games', views.games, name='games'),
    path('time/<int:additional>', views.time, name='time'),
    path('time', views.time, name='time'),
    path('matkul', views.matkul, name='matkul'),
    path('matkul/<int:id>', views.details, name='id'),
]
